package com.almis.awe.model.util.data;

import com.almis.awe.exception.AWException;
import com.almis.awe.model.dto.*;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.cfg.CoercionAction;
import com.fasterxml.jackson.databind.cfg.CoercionInputShape;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.type.LogicalType;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.NonNull;
import org.springframework.beans.PropertyAccessor;
import org.springframework.beans.PropertyAccessorFactory;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import java.lang.reflect.Field;
import java.util.*;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * DataList Class
 * Data list formatted as an standard data output
 *
 * @author Pablo GARCIA - 24/JUN/2010
 */
public final class DataListUtil {

  private static final String CANT_CREATE_INSTANCE = "Can't create instance of ";
  private static final ObjectMapper mapper = new Jackson2ObjectMapperBuilder()
    .failOnUnknownProperties(false)
    .defaultViewInclusion(false)
    .simpleDateFormat("yyyy-MM-dd@HH:mm:ss.SSSZ")
    .featuresToEnable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT)
    //.serializationInclusion(JsonInclude.Include.NON_EMPTY)
    .modules(new JavaTimeModule())
    .build();

  /**
   * Private constructor to hide the implicit one
   */
  private DataListUtil() {
    mapper.coercionConfigFor(LogicalType.Collection).setCoercion(CoercionInputShape.EmptyString, CoercionAction.AsNull);
  }

  /**
   * Retrieve object mapper
   * @return ObjectMapper
   */
  public static ObjectMapper getMapper() {
    return mapper;
  }

  /**
   * Returns the value data by (rowNumber, hashKey)
   *
   * @param list      DataList
   * @param rowNumber Row number
   * @param key       Column name
   * @return Single Value Data
   */
  public static String getData(DataList list, Integer rowNumber, String key) {
    // Variable definition
    Map<String, CellData> row;
    String cellString = "";

    row = Optional.ofNullable(getRow(list, rowNumber)).orElse(Collections.emptyMap());
    CellData cell = row.get(key);
    if (cell != null) {
      cellString = cell.getStringValue();
    }

    return cellString;
  }

  /**
   * Returns the cellData value by (rowNumber, columnName)
   *
   * @param list       DataList
   * @param rowNumber  Row number
   * @param columnName Column name
   * @return Single cellData
   */
  public static CellData getCellData(DataList list, Integer rowNumber, String columnName) {
    return Optional.ofNullable(getRow(list, rowNumber)).orElse(Collections.emptyMap()).get(columnName);
  }

  /**
   * Returns the HasMap Row by (rowNumber)
   *
   * @param list      DataList
   * @param rowNumber Row number
   * @return Single Hash Map
   */
  public static Map<String, CellData> getRow(DataList list, int rowNumber) {
    return Optional.ofNullable(list.getRows()).orElse(Collections.emptyList()).stream().skip(rowNumber).findFirst().orElse(null);
  }

  /**
   * Retrieve the row index for an identifier
   *
   * @param list             DataList
   * @param columnIdentifier Column identifier
   * @param rowIdentifier    Row identifier
   * @return Row index
   */
  public static int getRowIndex(DataList list, String columnIdentifier, Object rowIdentifier) {
    return IntStream.range(0, list.getRows().size())
      .filter(i -> rowIdentifier.equals(list.getRows().get(i).get(columnIdentifier).getValue()))
      .findFirst()
      .orElse(-1);
  }

  /**
   * Set column Name and it's value per line
   *
   * @param list         DataList
   * @param columnName   Column name (alias)
   * @param defaultValue Default value
   */
  public static void addColumn(DataList list, String columnName, String defaultValue) {
    CellData cell;

    // Add alias row by row
    for (Map<String, CellData> row : list.getRows()) {
      // Define cell
      cell = new CellData();

      // Add default value
      if (defaultValue != null) {
        cell.setValue(defaultValue);
      }

      // Store cell
      row.put(columnName, cell);
    }
  }

  /**
   * Copy column from other datalist
   *
   * @param target           Target datalist
   * @param targetColumnName Target column
   * @param source           Source datalist
   * @param sourceColumnName Source column
   */
  public static void copyColumn(DataList target, String targetColumnName, DataList source, String sourceColumnName) {
    addColumn(target, targetColumnName, getColumn(source, sourceColumnName));
  }

  /**
   * Rename column in datalist
   *
   * @param dataList         DataList
   * @param sourceColumnName Source column name
   * @param targetColumnName Target column name
   */
  public static void renameColumn(DataList dataList, String sourceColumnName, String targetColumnName) {
    if (dataList.getRows() != null) {
      for (Map<String, CellData> row : dataList.getRows()) {
        row.put(targetColumnName, row.get(sourceColumnName));
        row.remove(sourceColumnName);
      }
    }
  }

  /**
   * Set column Name and it's value per line
   *
   * @param list         Datalist
   * @param columnName   Column name (alias)
   * @param columnValues List with column values
   */
  public static void addColumn(DataList list, String columnName, List<?> columnValues) {
    Integer rowIdentifier = 0;

    // Add alias row by row
    for (Object columnData : columnValues) {
      CellData cell;
      if (columnData instanceof CellData cellData) {
        cell = cellData;
      } else {
        cell = new CellData(columnData);
      }

      // Add cell to row
      addCellToRow(list, columnName, rowIdentifier, cell);

      // Increase row identifier
      rowIdentifier++;
    }
  }

  /**
   * Set column Name and it's value per line
   *
   * @param list      DataList
   * @param column    Column name
   * @param rowNumber Row number
   * @param cell      Cell data
   */
  private static void addCellToRow(DataList list, String column, Integer rowNumber, CellData cell) {
    // If size is lower or equal than current rowId, add one row
    if (list.getRows().size() <= rowNumber) {
      list.getRows().add(new HashMap<>());
    }

    // Store cell
    list.getRows().get(rowNumber).put(column, cell);

    // Set records
    list.setRecords(list.getRows().size());
  }

  /**
   * Returns the datalist as a string array
   *
   * @param dataList   Datalist
   * @param columnList Column order
   * @return DataList as a string array
   */
  public static String[] getDataAsArray(DataList dataList, List<String> columnList) {
    List<String> list = new ArrayList<>();

    for (Map<String, CellData> row : dataList.getRows()) {
      for (String val : columnList) {
        CellData cell = row.get(val);
        list.add(cell != null ? cell.getStringValue() : "");
      }
    }
    String[] dataArray = new String[list.size()];
    return list.toArray(dataArray);
  }

  /**
   * Return the datalist as bean list
   *
   * @param dataList  datalist
   * @param beanClass bean class
   * @param <T>       class type
   * @return bean list
   */
  public static <T> List<T> asBeanList(@NonNull DataList dataList, Class<T> beanClass) {
    return dataList.getRows().stream()
      .map(row -> mapper.convertValue(row, beanClass))
      .toList();
  }

  /**
   * Retrieve parameter as bean value from JSON. You can use Spring Formatter SPI with annotations
   *
   * @param beanClass Bean class
   * @param paramsMap Parameter map
   * @param <T>       Bean type
   * @return Bean value
   */
  public static <T> T getParameterBeanValue(Class<T> beanClass, Map<String, Object> paramsMap) {
    return mapper.convertValue(paramsMap, beanClass);
  }

  /**
   * Retrieve parameter as bean list value. You can use Spring Formatter SPI with annotations.
   *
   * @param beanClass Bean class
   * @param paramsMap Parameter map
   * @return Bean list
   */
  public static <T> List<T> getParameterBeanListValue(Class<T> beanClass, Map<String, Object> paramsMap) {
    return fixParamMap(paramsMap)
      .stream()
      .map(row -> getParameterBeanValue(beanClass, row))
      .toList();
  }

  private static List<Map<String, Object>> fixParamMap(Map<String, Object> paramsMap) {
    // Get max length of list
    int maxSize = paramsMap.values().stream()
      .filter(List.class::isInstance)
      .max(Comparator.comparing(o -> ((List<?>) o).size()))
      .map(o -> ((List<?>) o).size())
      .orElse(1);

    // Get each value of the map in a list
    return IntStream.range(0, maxSize)
      .mapToObj(i -> paramsMap.entrySet().stream()
        .collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue() instanceof List<?> ? ((List<?>) e.getValue()).get(i) : e.getValue() )))
      .toList();
  }

  /**
   * Initialize bean list
   *
   * @param valueList Value list of field
   * @param beanClass Bean class
   * @param <T>       bean class
   * @return Initialized bean list
   * @throws AWException AWE exception
   */
  public static <T> List<T> initializeList(List<T> valueList, Class<T> beanClass) throws AWException {
    List<T> beanList = new ArrayList<>();
    // Initialize list if first defined
    if (!valueList.isEmpty()) {
      for (int i = 0, t = valueList.size(); i < t; i++) {
        // Initialize list
        try {
          // Generate row bean
          T parameterBean = beanClass.getConstructor().newInstance();
          beanList.add(parameterBean);
        } catch (Exception exc) {
          throw new AWException("Error converting datalist into a bean list", CANT_CREATE_INSTANCE + beanClass.getSimpleName(), exc);
        }
      }
    }

    return beanList;
  }

  /**
   * Return the datalist as bean list
   *
   * @param beanList bean class
   * @param <T>      class type
   * @return bean list
   */
  public static <T> DataList fromBeanList(List<T> beanList) {
    DataList dataList = DataList.builder().build();

    for (T bean : beanList) {
      Map<String, CellData> row = new HashMap<>();
      PropertyAccessor rowBeanAccesor = PropertyAccessorFactory.forDirectFieldAccess(bean);

      // Set field value if found in row
      for (Field field : getAllFields(bean.getClass())) {
        row.put(field.getName(), new CellData(rowBeanAccesor.getPropertyValue(field.getName())));
      }

      dataList.addRow(row);
    }
    return dataList;
  }

  private static List<Field> getAllFields(Class<?> type) {
    List<Field> fields = new ArrayList<>(Arrays.asList(type.getDeclaredFields()));

    if (type.getSuperclass() != null) {
      fields.addAll(getAllFields(type.getSuperclass()));
    }

    return fields;
  }

  /**
   * Add a column with one row value
   *
   * @param list       DataList
   * @param columnName Column name (alias)
   * @param cellValue  Cell value
   */
  public static void addColumnWithOneRow(DataList list, String columnName, Object cellValue) {
    // Add cell to row
    addCellToRow(list, columnName, 0, new CellData(cellValue));
  }

  /**
   * Retrieve a column data
   *
   * @param list       DataList
   * @param columnName Column name (alias)
   * @return Column object list
   */
  public static List<CellData> getColumn(DataList list, String columnName) {
    // Add alias row by row
    return list.getRows().stream().map(row -> new CellData(row.get(columnName))).toList();
  }

  /**
   * Retrieve a column data as QueryParameter
   *
   * @param list       DataList
   * @param columnName Column name (alias)
   * @return Column object list
   */
  public static ArrayNode getColumnAsArrayNode(DataList list, String columnName) {
    ArrayNode arrayNode = JsonNodeFactory.instance.arrayNode();

    // Add alias row by row
    list.getRows().forEach(row -> arrayNode.add(mapper.valueToTree(row.get(columnName))));

    return arrayNode;
  }

  /**
   * Sort datalist
   *
   * @param list     DataList to sort
   * @param sortList Sort by field list
   */
  public static void sort(DataList list, List<SortColumn> sortList) {
    list.getRows().sort(new CompareRow(sortList));
  }

  /**
   * Sort datalist
   *
   * @param list      DataList to sort
   * @param columnId  Sort by field list
   * @param direction Sort direction
   */
  public static void sort(DataList list, String columnId, String direction) {
    List<SortColumn> sortColumnList = new ArrayList<>();
    SortColumn sortColumn = new SortColumn(columnId, direction);
    sortColumnList.add(sortColumn);
    list.getRows().sort(new CompareRow(sortColumnList));
  }

  /**
   * Sort datalist set nulls values position
   *
   * @param list           DataList to sort
   * @param sortColumnList List with sort columns
   * @param nullsFirst     Null values at first
   */
  public static void sort(DataList list, List<SortColumn> sortColumnList, boolean nullsFirst) {
    list.getRows().sort(new CompareRow(sortColumnList, nullsFirst));
  }

  /**
   * Remove the rows whose column value is distinct to the value
   *
   * @param list   DataList to filter
   * @param column column to check
   * @param value  value to check
   */
  public static void filter(DataList list, String column, String value) {
    filter(list, new FilterColumn(column, value));
  }

  /**
   * Remove the rows whose column value is distinct to the value
   *
   * @param list          DataList to filter
   * @param filterColumns columns to check
   */
  public static void filter(DataList list, FilterColumn... filterColumns) {
    filterDataList(list, String::equalsIgnoreCase, filterColumns);
  }

  /**
   * Remove the rows whose column value doesn't contains the defined values
   *
   * @param list          DataList to filter
   * @param filterColumns columns to check
   */
  public static void filterContains(DataList list, FilterColumn... filterColumns) {
    filterDataList(list, (String a, String b) -> a.toUpperCase().contains(b.toUpperCase()), filterColumns);
  }

  /**
   * Filter a DataList
   *
   * @param list          DataList to filter
   * @param comparator    Comparator
   * @param filterColumns Columns to filter
   */
  private static void filterDataList(DataList list, BiPredicate<String, String> comparator, FilterColumn... filterColumns) {
    List<Map<String, CellData>> newRows = new ArrayList<>();
    for (Map<String, CellData> row : list.getRows()) {
      boolean add = false;
      for (FilterColumn filterColumn : filterColumns) {
        if (row.containsKey(filterColumn.getColumnId()) && comparator.test(row.get(filterColumn.getColumnId()).getStringValue().toUpperCase(), filterColumn.getValue().toUpperCase())) {
          add = true;
        }
      }

      if (add) {
        newRows.add(row);
      }
    }
    // Set the new list
    list.setRows(newRows);
    list.setRecords(newRows.size());
  }

  /**
   * Retrieve dataList column names
   *
   * @param list Datalist
   * @return Column list
   */
  public static List<String> getColumnList(DataList list) {
    // Get first row
    return new ArrayList<>(Optional.ofNullable(getRow(list, 0)).orElse(Collections.emptyMap()).keySet());
  }

  /**
   * Retrieve dataList column names
   *
   * @param list Datalist
   * @return Column list
   */
  public static boolean hasColumn(DataList list, String columnId) {
    // Get first row
    return getColumnList(list).contains(columnId);
  }

  /**
   * Keeps only distinct values of given fields
   *
   * @param list        DataList
   * @param sortColumns Sort by field list
   */
  public static void distinct(DataList list, List<SortColumn> sortColumns) {
    CompareRow comparator = new CompareRow(sortColumns);
    List<Map<String, CellData>> newRows = new ArrayList<>();
    for (Map<String, CellData> row : list.getRows()) {
      if (!in(newRows, row, comparator)) {
        newRows.add((Map<String, CellData>) ((HashMap<String, CellData>) row).clone());
      }
    }

    // Set rows and records
    list.setRows(newRows);
    list.setRecords(list.getRows().size());
  }

  /**
   * Check if a row is inside another list
   *
   * @param list       Row list
   * @param rowToCheck row to check
   * @param comparator row comparator
   */
  private static boolean in(List<Map<String, CellData>> list, Map<String, CellData> rowToCheck, CompareRow comparator) {
    return list.stream().anyMatch(row -> comparator.compare(row, rowToCheck) == 0);
  }
}
