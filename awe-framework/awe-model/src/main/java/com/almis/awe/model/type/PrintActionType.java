package com.almis.awe.model.type;

public enum PrintActionType {
  MAIL,
  CREATE
}
