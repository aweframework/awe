package com.almis.awe.service;

import com.almis.awe.config.ServiceConfig;
import com.almis.awe.exception.AWException;
import com.almis.awe.model.entities.services.Service;
import com.almis.awe.model.type.LaunchPhaseType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;

import java.util.*;

import static com.almis.awe.model.constant.AweConstants.DOUBLE_LOG_LINE;
import static com.almis.awe.model.constant.AweConstants.LOG_LINE;

/**
 * Manage application initialization
 */
@Slf4j
public class InitService extends ServiceConfig implements DisposableBean {

  // Autowired services
  private final LauncherService launcherService;

  /**
   * Autowired constructor
   *
   * @param launcherService Launcher service
   */
  public InitService(LauncherService launcherService) {
    this.launcherService = launcherService;
  }

  /**
   * Launch services when application have been started (before spring context)
   *
   * @param event Context refresh event
   */
  @EventListener
  public void onApplicationStart(ContextRefreshedEvent event) {
    log.info(DOUBLE_LOG_LINE);
    log.info("=======  Launching application start services  =======");
    log.info(DOUBLE_LOG_LINE);
    launchPhaseServices(LaunchPhaseType.APPLICATION_START);
    log.info(LOG_LINE);
    log.info("-----------------------------  AWE STARTED  --------------------------------------");
    log.info(LOG_LINE);
  }

  /**
   * Launch services when application have been started (before spring context)
   */
  public void destroy() throws Exception {
    log.info(DOUBLE_LOG_LINE);
    log.info("=======  Launching application end services  =========");
    log.info(DOUBLE_LOG_LINE);
    launchPhaseServices(LaunchPhaseType.APPLICATION_END);
    log.info(LOG_LINE);
    log.info("-----------------------------  AWE STOPPING  -------------------------------------");
    log.info(LOG_LINE);
  }

  /**
   * Launch a client initialization
   */
  public void onClientStart() {
    // Launch client start services
    log.info(DOUBLE_LOG_LINE);
    log.info("=======  Initializing client start services  =========");
    log.info(DOUBLE_LOG_LINE);
    launchPhaseServices(LaunchPhaseType.CLIENT_START);
  }

  /**
   * Launch initial services
   *
   * @param phase Service phase
   */
  public void launchPhaseServices(LaunchPhaseType phase) {
    List<Service> serviceList;

    try {
      serviceList = getElements().getPhaseServices(phase);
      switch (phase) {
        case APPLICATION_START:
        case CLIENT_START:
          // Search from awe to application file
          Collections.reverse(serviceList);
          break;
        default:
          // Search from application file to awe
      }
    } catch (AWException exc) {
      log.error(exc.getMessage(), exc);
      serviceList = new ArrayList<>();
    }

    // Launch service list
    launchServiceList(serviceList);
  }

  /**
   * Launch a service list
   */
  private void launchServiceList(List<Service> serviceList) {
    Map<String, Object> parameters = new HashMap<>();
    for (Service service : serviceList) {
      try {
        log.info("\tLaunching initial service: [\u001B[32m{}\u001B[0m]", service.getId());
        launcherService.callService(service.getId(), parameters);
      } catch (AWException exc) {
        log.error(exc.getMessage(), exc);
      }
    }
  }
}
